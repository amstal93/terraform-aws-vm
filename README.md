# terraform-aws-vm

Example of provision of an alpine linux image into AWS EC2

### Prerequisites

* Have AWS account (Free tier) [link][aws_free]
* An access key in IAM with enough permissions to create resources [link][aws_iam]
* S3 bucket `{proyect}-terraform-state-files` [link][aws_s3]
* Terraform cli `>= 0.12.20`
* Public/private ssh key pair

## STEPS

#### 1. Clone the respository: https://gitlab.com/enriqueperez/infrastructure/terraform-aws-vm.git
```bash
git clone https://gitlab.com/enriqueperez/infrastructure/terraform-aws-vm.git
cd terraform-aws-vm
```

#### 2. set environment vars and `{repo}/config/vars.tfvars`

Windows console:
***
```
setx AWS_DEFAULT_REGION "{region}"
setx AWS_ACCESS_KEY_ID "{aws_key}"
setx AWS_SECRET_ACCESS_KEY "{aws_secret}"
```
**Note**: values won't be visible in current prompt. Reopen terminal for this.

Linux/Mac terminal:
***
```bash
export AWS_DEFAULT_REGION="{region}"
export AWS_ACCES_KEY_ID="{aws_key}"
export AWS_SECRET_ACCESS_KEY="{aws_secret}"
```

#### 4. add your private ssh key into `{repo}/config/private-key`

#### 5. Init the backend
```bash
terraform init
```

#### 6. Show the plan
```bash
terraform plan -var-file=config/vars.tfvars
```

#### 7. Build the infra with apply
```bash
terraform apply -var-file=config/vars.tfvars

```
### Description

Terraform will create 5 resources:

- `aws_key_pair`-> key pair to access via ssh to instance.
- `aws_eip.eip`-> A public elastic ip address.
- `aws_security_group`-> Security group with firewall access definition.
- `aws_instance`-> The instance.
- `aws_eip_association`-> Association between instance and the Elastic IP.

Current state of plan will be stored into S3 bucket previously created

Files:

- `variables.tf` contains the initialization of empty variables required.
- `config/vars.tfvars` the values of the vars defined.
- `backend.tf` the terraform client and backend configuration for state files.
- `provider.tf` contains the config of aws plugin to handle amazon resources.
- `network.tf` contains network resources
- `nginx.tf` contains all definitions to create the instance and provision config.
- `.gitignore` contains definitions to avoid pushing credentials or terraform files
- `output.tf` contains the vars output defs to shown after apply.
- `config` folder contains the configuration and keys.
- `config/bootstrap/content` for custom website, add and replace your own files in here.

### Test

* Browser: `http://{elastic_ip}`

![](config/browser_image.png)

* SSH: connect to terminal `ssh -i {yourprivatekey} alpine@{elastic_ip}`

## How to Uninstall

Destroy the resources with a single terraform command
```bash
terraform destroy -var-file=config/vars.tfvars
```

[aws_iam]: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html
[aws_free]: https://aws.amazon.com/us/free/
[aws_s3]: https://docs.aws.amazon.com/quickstarts/latest/s3backup/step-1-create-bucket.html
